import React from 'react';
import withStyles from 'react-jss';

const styles = {
    button: {
        display: 'inline-block',
        padding: '6px 12px',
        border: '2px solid #ddd',
        color: '#ddd',
        textDecoration: 'none',
        fontWeight: 700,
        textTransform: 'uppercase',
        margin: '15px 5px 0',
        transition: 'all .2s',
        minWidth: '100px',
        textAlign: 'center',
        fontSize: '14px',
        '&:hover': {
            border: '2px solid #a0dd4c',
            color: '#a0dd4c'
        },
    },
};

const Button = ({ classes, children }) => (
    <button className={classes.button}>
        {children}
    </button>
);

export default withStyles(styles)(Button);
import React from 'react';
import withStyles from 'react-jss';

const styles = {
    '@keyframes loading': {
        from: {
            top: '32px',
            left: '32px',
            width: 0,
            height: 0,
            opacity: 1
        },
        to: {
            top: '0px',
            left: '0px',
            width: '64px',
            height: '64px',
            opacity: 0
        }
    },
    loading: {
        display: 'inline-block',
        position: 'relative',
        width: '64px',
        height: '64px',
        '& div': {
            position: 'absolute',
            border: '2px solid #bbb',
            opacity: 1,
            borderRadius: '50%',
            animation: `loading 1s cubic-bezier(0, 0.25, 0.75, 1) infinite`,
        },
        '& div:nth-child(2)': {
            animationDelay: '-0.15s'
        },
        '& div:nth-child(3)': {
            animationDelay: '-0.3s'
        }
    }
};

const Loading = ({ classes }) => {
    return(
        <div className={classes.loading}>
            <div />
            <div />
            <div />
        </div>
    )
};

export default withStyles(styles)(Loading);
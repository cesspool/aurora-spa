import React from 'react';
import withStyles from 'react-jss';
import PropTypes from 'prop-types';

const styles = {
    progressBar: {
        position: 'relative',
        height: '20px',
        width: '350px',
        borderRadius: '50px',
        border: '1px solid #333'
    },
    filler: {
        background: '#1da598',
        height: '100%',
        borderRadius: 'inherit',
        transition: 'width .2s ease-in',
    }
};

const Filler = ({ classes, percentage }) => {
    return(
        <div className={classes.filler} style={{ width: `${percentage}%` }} />
    )
};

export const StyledFiller = withStyles(styles)(Filler);

const ProgressBar = ({ classes, percentage }) => {
    return(
        <div className={classes.progressBar}>
            <StyledFiller percentage={percentage} />
        </div>
    )
};

ProgressBar.propTypes = {
    percentage: PropTypes.number.isRequired
};


export default withStyles(styles)(ProgressBar);
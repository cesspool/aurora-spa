import React from 'react';
import withStyles from 'react-jss';

const styles = {
    header: {
        paddingTop: '10px',
        paddingBottom: '60px',
        background: 'url(../../assets/image/background.webp)',
        height: '750px',
        backgroundSize: 'cover',
        webkitBackgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        padding: 0,
        '&::after': {
            background: 'rgba(0, 0, 0, 0.75)',
            position: 'absolute',
            height: '100%',
            width: '100%'
        }
    },
};

const LandingImage = ({ classes }) => {
    return(
        <section className={classes.header} id="home">
            <div className={classes.overlay} />
        </section>
    )
};

export default withStyles(styles)(LandingImage);
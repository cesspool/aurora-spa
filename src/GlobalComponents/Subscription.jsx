import React from 'react';
import withStyles from 'react-jss';
import Panel from './Panel/Panel';
import PanelHeader from "./Panel/PanelHeader";
import PanelFooter from "./Panel/PanelFooter";
import PanelBody from "./Panel/PanelBody";
import Button from "./UI/Button";

const styles = theme => ({
    dialog: {
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        background: 'rgba(0,0,0,0.6)'
    },
    dialogMain: {
        position: 'fixed',
        background: 'white',
        width: '40%',
        height: 'auto',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%,-50%)'
    },
    displayBlock: {
        display: 'block'
    },
    displayNone: {
        display: 'none'
    }
});

const SubscriptionDialog = ({ classes, handleClose, isOpen, children }) => {

    const openDialog = isOpen ? classes.dialog && classes.displayBlock : classes.dialog && classes.displayNone;

    return(
        <div className={openDialog}>
            <Panel className={classes.dialogMain}>
                <PanelHeader>Subscribe to aurora notifications:</PanelHeader>
                <PanelBody>
                    <p>testi</p>
                </PanelBody>
                <PanelFooter>
                    <button onClick={handleClose}>CLOSE</button>
                    <Button>CLOSE</Button>
                </PanelFooter>
            </Panel>
        </div>
    )
};

export default withStyles(styles)(SubscriptionDialog);
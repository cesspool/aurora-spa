import React from 'react';
import withStyles from 'react-jss';

const styles = {
    panelFooter: {
        backgroundColor: '#282828',
        padding: '.75rem 1.25rem',
        borderTop: '1px solid rgba(0,0,0,.125)'
    }
};

const PanelFooter = ({ classes, children }) => (
    <div className={classes.panelFooter}>
        {children}
    </div>
);

export default withStyles(styles)(PanelFooter);
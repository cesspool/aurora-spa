import React from 'react';
import withStyles from 'react-jss';

const styles = {
    panelBody: {
        flex: '1 1 auto',
        padding: '1.25rem'
    }
};

const PanelBody = ({ classes, children }) => (
    <div className={classes.panelBody}>
        {children}
    </div>
);

export default withStyles(styles)(PanelBody);
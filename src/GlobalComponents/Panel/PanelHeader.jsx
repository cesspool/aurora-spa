import React from 'react';
import withStyles from 'react-jss';

const styles = {
    panelHeader: {
        backgroundColor: '#282828',
        padding: '.75rem 1.25rem',
        marginBottom: 0,
        borderBottom: '1px solid rgba(0,0,0,.125)'
    }
};

const PanelHeader = ({ classes, children }) => (
    <div className={classes.panelHeader}>
        {children}
    </div>
);

export default withStyles(styles)(PanelHeader);
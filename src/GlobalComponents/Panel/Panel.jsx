import React from 'react';
import withStyles from 'react-jss';

const styles = {
    panel: {
        marginTop: '2rem',
        color: '#747474',
        backgroundColor: '#313131',
        boxShadow: '0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12)',
        position: 'relative',
        display: 'flex -webkit-flex',
        flexDirection: 'column',
        webkitFlexDirection: 'column',
        minWidth: 0,
        wordWrap: 'break-word',
        backgroundClip: 'border-box',
        border: '1px solid rgba(0,0,0,.125)',
        borderRadius: '.25rem',
        flex: '1 1 auto',
        webkitFlex: '1 1 auto'
    }
};

const Panel = ({ classes, children }) => (
    <div className={classes.panel}>
        {children}
    </div>
);

export default withStyles(styles)(Panel);
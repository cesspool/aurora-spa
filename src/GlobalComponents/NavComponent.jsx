import React, {Component} from 'react';
import { NavLink } from 'react-router-dom';
import socketIOClient from 'socket.io-client';
import Subscription from './Subscription';

var socket;

class NavComponent extends Component {

    constructor() {
        super();
        this.state = {
            endpoint: 'http://localhost:3000',
            isOpen: false
        };
        socket = socketIOClient(this.state.endpoint);
    }

    handleDialogOpen = () => {
        this.setState({isOpen: true})
    };

    handleDialogClose = () => {
        this.setState({isOpen: false})
    };

    render() {

        return(
                    <header className="nav-bar">
                        <div className="container">
                            <a href="https://www.northerneye.live" className="logo"><img src="../image_content/main-logo.svg" width="200px" alt="Northern Eye" /></a>
                            <NavLink to="/" className="button">HOME</NavLink>
                            <button className="button"><NavLink to="/about">ABOUT AURORAS</NavLink></button>
                            <button onClick={this.handleDialogOpen}>Subscribe</button>
                            <Subscription handleClose={this.handleDialogClose} isOpen={this.state.isOpen}/>
                        </div>
                    </header>
        )
    }
}

export { NavComponent, socket };

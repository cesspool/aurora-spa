import React, { Component } from 'react';
import { Switch, Route } from "react-router-dom";
import { NavComponent } from './GlobalComponents/NavComponent';
import AuroraComponent from "./MainComponents/AuroraComponent";
import ChatComponent from "./MainComponents/ChatComponent";
import AboutComponent from "./MainComponents/AboutComponent";
import HomeComponent from "./Home/HomeComponent";
import WeatherContainer from './Weather';
import NotFound from './NotFound/NotFound';

class App extends Component {

    componentDidMount() {


        function positionSuccess(position) {
        }

        function positionError() {
            this.setState({status: 'Can not fetch'});
        }

        this.setState({status: 'Asking location' });

        navigator.geolocation.getCurrentPosition(positionSuccess, positionError);
    }


    constructor(props) {
        super(props);
        this.state = {
            coordsData: null
        };
    };

    coordsCallback = (coordinatesData) => {
        this.setState({coordsData: coordinatesData});
        var coords = (coordinatesData['coords']);
        return console.log(coords);
    };


  render() {

    return (
      <div className="App">
          <NavComponent />
          <Switch>
              <Route exact path="/" component={ HomeComponent } />
              <Route exact path="/about" component={ AboutComponent } />
              <Route component={ NotFound } />
          </Switch>


          {/*
          <WeatherComponent callback={this.coordsCallback} />
          <MapComponent coordsData={this.state.coordsData} />
          <ChatComponent />
          */}

      </div>
    );
  }
}

export default App;

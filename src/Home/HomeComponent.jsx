import React, { Component } from 'react';
import AuroraComponent from '../MainComponents/AuroraComponent';
import ChatComponent from '../MainComponents/ChatComponent';
import Weather from "../Weather";
import LandingImage from "../GlobalComponents/UI/LandingImage";

class HomeComponent extends Component {
    render() {
        return(
            <div>
                <LandingImage />
                <AuroraComponent />
                <Weather />
                <ChatComponent />
            </div>
        )
    }
}

export default HomeComponent;

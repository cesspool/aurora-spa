import React, { Component } from 'react';
import WeatherData from "./WeatherData";
import MapComponent from '../MainComponents/MapComponent';
import Loading from "../GlobalComponents/UI/Loading";

class Weather extends Component {

    constructor() {
        super();
        this.state = {
            status: null,
            loading: false,
            data: null,
            position: null
        };
    }

    getWeather = () => {

        const that = this;
            function positionSuccess(position) {

                that.setState({position: position});

                fetch('http://localhost:3000/weatherAPI/'
                    + position.coords.latitude
                    + ','
                    + position.coords.longitude)
                    .then(response => response.json())
                    .then(data => that.setState({data: data}))
                    .then(that.setState({ loading: false }))
            }
            function positionError() {
            }
            navigator.geolocation.getCurrentPosition(positionSuccess, positionError);
            this.setState({ loading: true })
        };

    render() {

        let loading;
        if (this.state.loading) {
            return loading = <Loading />;
        }

        let weatherData;
        if(this.state.data) {
            weatherData = <WeatherData data={this.state.data.data} />
        }

        return (
            <div className="container">
                <div className="panel">
                    <div className="panel-header">
                        <div className="row">
                            <img id="weather-logo" src={'./image_content/weather.svg'} alt={'5 hour weather'} height={'30px'} />
                            <button onClick={this.getWeather} className="button u-pull-right">GET WEATHER</button>
                        </div>
                    </div>
                    <div className="panel-body">
                        {loading}
                        <p>Click GET WEATHER to get local weather information</p>
                        {weatherData}
                    </div>
                    <div className="panel-footer" />
                </div>
                <MapComponent position={this.state.position} />
            </div>
        )
    }
}

export default Weather;

import React from 'react';

const WeatherData = ({data}) => {

    const columns = [];

    for(let dataIndex = 0; dataIndex < 5; dataIndex++) {

        let weather = data[dataIndex];

        columns.push(
            <WeatherBlock key={dataIndex} weather={weather} />
        )
    }
    return(
        <div className="row">
            {columns}
        </div>
    )
};

const WeatherBlock = ({weather}) => {

    function timeConvert(value) {
        const conversion = new Date(value * 1000);
        return conversion.toLocaleString()
    }

    return(
        <div className="weather two columns">
            <img src={'./image_content/weather_icons/clear-day.svg'} alt={'weather-ico'} width={'50px'} />
            <hr />
            <p>{timeConvert(weather.time)}</p>
            <p>{weather.summary}</p>
            <p>{Math.round(weather.temperature) + ' °C'}</p>
        </div>
    )
};

export default WeatherData;

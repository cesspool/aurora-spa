import React, { Component } from 'react';
import Map from 'pigeon-maps';
import Marker from 'pigeon-marker';

class MapComponent extends Component {

    render() {

            return (
                <div className="container">
                    <div className="panel" text="white" color="elegant-color" style={{marginTop: "1rem"}}>
                        <div className="panel-header"><div className="row"><img id="map-logo" src={'./image_content/map.svg'} alt={'Weather-location'} height={'30px'}></img></div></div>
                        <div className="panel-body" id="map-card">
                            <div id={'map'} style={{ height: '50vh', width: '100%' }}>
                                <Map center={[66.4826, 25.7194]} zoom={12}>
                                    <Marker anchor={[66.4826, 25.7194]} payload={1} onClick={({ event, anchor, payload }) => {}} />
                                </Map>
                            </div>
                        </div>
                        <div className="panel-footer">
                        </div>
                    </div>
                </div>
            )
        }
    }

export default MapComponent;

import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from 'react-jss';
import './index.css';
import './skeleton.css';
import './normalize.css';
import './App.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from "react-router-dom";

const theme = {
    primaryColor: '#222222'
};

ReactDOM.render(
    <BrowserRouter>
       <ThemeProvider theme={theme}>
            <App />
       </ThemeProvider>
    </BrowserRouter>,
    document.getElementById('root'));
   
  

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();

import React, { Component } from 'react';

class NotFound extends Component {
    render() {
        return(
            <div className="container">
            <div className="panel">
                <div className="panel-header"></div>
                <div className="panel-body"><code>404 NO AURORAS HERE 404</code></div>
                <div className="panel-footer"></div>
            </div>
            </div>
        )
    }
}

export default NotFound;

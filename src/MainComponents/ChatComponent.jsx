import React, { Component } from 'react';
//import { socket } from '../GlobalComponents/NavComponent'

class ChatComponent extends Component {


    render() {

        return (

            <div className="container">
                <div className="panel">
                        <div className="panel-header"><img id="chat-logo" src={'./image_content/aurora-chat.svg'} alt={'Aurora chat'} height={'30px'}></img></div>
                    <div className="panel-body">
                        <form>
                        <div className="row">
                            <label htmlFor="chatField">Chat
                                <textarea className="u-full-width" placeholder="" id="chatField"></textarea>
                            </label>
                            <div className="six columns">
                                <label htmlFor="exampleEmailInput">Nickname</label>
                                <input className="u-full-width" type="email" placeholder="keijojäbä :--D"
                                       id="exampleEmailInput">
                                </input>
                            </div>
                        </div>
                        <label htmlFor="exampleMessage">Message
                        <textarea className="u-full-width" placeholder="Type your message" id="exampleMessage">
                        </textarea>
                        </label>
                        <button className="button" type="submit" value="Submit">SUBMIT
                        </button>
                        </form>
                    </div>
                    <div className="panel-footer">
                    </div>
                </div>
            </div>
        )
    }
}

export default ChatComponent;

import React, {Component} from 'react';
import Loading from "../GlobalComponents/UI/Loading";

class WeatherComponent extends Component {

    constructor() {
        super();
        this.getPosition = this.getPosition.bind(this);
    }

    state = {
        measurement: [],
    };


    componentDidMount() {
        const that = this;
        function positionSuccess(position) {

            that.setState({status: 'Fetching... '});

            fetch('http://localhost:5051/forecast/'
                + position.coords.latitude
                + ','
                + position.coords.longitude)

                .then(response => response.json())
                .then(data => that.setState({data: data, status: ''}));
        }

        function positionError() {
            this.setState({status: 'Can not fetch'});
        }

        this.setState({status: 'Asking location' });

        navigator.geolocation.getCurrentPosition(positionSuccess, positionError);
    }

    getPosition() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(pos) {
                console.log("Latitude: " + pos.coords.latitude +
                    "Longitude: " + pos.coords.longitude);
            });
        } else {
            console.log("Geolocation is not supported by this browser.");
        }
    }

    render() {

        let loader;
        if (!this.state.data) {
            return loader = <Loading />;
        }

        let weatherDataHour = (this.state.data['hourly'].data);

        let clockTimeUnix = [];
        for(let i = 0; i < 5; i++) {
            clockTimeUnix.push(weatherDataHour[i].time)
        }

        let clockTimeHuman = clockTimeUnix.map(timeConvert);

        function timeConvert(value) {
            let conversion = new Date(value * 1000);
            return conversion.toLocaleString('gb-GB')
        }

        return (

            <div className="container">
                <div className="panel" text="white" style={{ marginTop: "1rem" }}>
                    <div className="panel-header"><div className="row"><img id="weather-logo" src={'./image_content/weather.svg'} alt={'5 hour weather'} height={'30px'} />
                        <button id="get-weather" onClick={this.getPosition} className="button u-pull-right">GET WEATHER</button>
                    </div></div>
                    <div className="panel-body" color="lighter-color">
                        <div className="row justify-content-between">
                            <div className="weather two columns">
                                <img id="current-icon" src={'./image_content/weather_icons/' + weatherDataHour[0].icon + '.svg'} alt={''} width={'50px'}></img>
                                <hr/>
                                <p>
                                    {clockTimeHuman[0]}
                                </p>
                                <p>
                                    {weatherDataHour[0].summary}
                                </p>
                                <p>
                                    {Math.round(weatherDataHour[0].temperature) + ' °C'}
                                </p>
                            </div>
                            <div className="weather two columns">
                                <img src={'./image_content/weather_icons/' + weatherDataHour[0].icon + '.svg'} alt={''} width={'50px'}></img>
                                <hr/>
                                <p>
                                    {clockTimeHuman[1]}
                                </p>
                                <p>
                                    {weatherDataHour[0].summary}
                                </p>
                                <p>
                                    {Math.round(weatherDataHour[1].temperature) + ' °C'}
                                </p>
                            </div>
                            <div className="weather two columns">
                                <img src={'./image_content/weather_icons/' + weatherDataHour[0].icon + '.svg'} alt={''} width={'50px'}></img>
                                <hr/>
                                <p>
                                    {clockTimeHuman[2]}
                                </p>
                                <p>
                                    {weatherDataHour[0].summary}
                                </p>
                                <p>
                                    {Math.round(weatherDataHour[2].temperature) + ' °C'}
                                </p>
                            </div>
                            <div className="weather two columns">
                                <img src={'./image_content/weather_icons/' + weatherDataHour[0].icon + '.svg'} alt={''} width={'50px'}></img>
                                <hr/>
                                <p>
                                    {clockTimeHuman[3]}
                                </p>
                                <p>
                                    {weatherDataHour[0].summary}
                                </p>
                                <p>
                                    {Math.round(weatherDataHour[3].temperature) + ' °C'}
                                </p>
                            </div>
                            <div className="weather two columns">
                                <img src={'./image_content/weather_icons/' + weatherDataHour[0].icon + '.svg'} alt={''} width={'50px'}></img>
                                <hr/>
                                <p>
                                    {clockTimeHuman[4]}
                                </p>
                                <p>
                                    {weatherDataHour[0].summary}
                                </p>
                                <p>
                                    {Math.round(weatherDataHour[4].temperature) + ' °C'}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="panel-footer"><a href="https://darksky.net/poweredby/"><img src={'./image_content/dark-sky.svg'} alt={'Weather API by Dark Sky'} height={'25px'}></img></a></div>
                </div>
            </div>
        );
    }
}
export default WeatherComponent;

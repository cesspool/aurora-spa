import React, { Component } from 'react';
import { socket } from '../GlobalComponents/NavComponent';
import {
    AreaChart,
    Area,
    LineChart,
    Line,
    CartesianGrid,
    XAxis,
    YAxis,
    Legend,
    Tooltip,
    ResponsiveContainer,
    ReferenceLine, Label
} from 'recharts';
import Panel from "../GlobalComponents/Panel/Panel";
import PanelHeader from "../GlobalComponents/Panel/PanelHeader";
import PanelFooter from "../GlobalComponents/Panel/PanelFooter";
import ProgressBar from '../GlobalComponents/UI/ProgressBar';
import Loading from '../GlobalComponents/UI/Loading';

class AuroraComponent extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            measurement_data: [],
            fmi_data: [],
            data: [],
            notificationData: 0,
            percentage: 0
        }
    }

    getData = measurementData => {
        this.setState({measurement_data: measurementData, percentage: 100});
    };

    getFmi = fmiData => {
        this.setState({fmi_data: fmiData});
    };

    changeData = () => socket.emit("initial_data");

    componentDidMount() {
        //var state_current = this;
        socket.emit("initial_data");
        socket.on("get_data", this.getData);
        socket.on('fmi_data', this.getFmi);
        socket.on("change_data", this.changeData);
    }

    componentWillUnmount() {
        socket.off('get_data', this.getData);
    }


    render() {

        if(!this.state.measurement_data && !this.state.fmi_data) {
            return <div>Loading...</div>
        }

        let fmiMeasData = this.state.fmi_data;
        var measData = this.state.measurement_data;

        var data = this.state.measurement_data;
        let fmiDatat = this.state.fmi_data;

        console.log(data);

        /*
        for(var i = 0; i < 10; i++) {
            let slicedFmi = fmiMeasData.slice(Math.max(fmiMeasData.length - 10, 1));
            //var slicedData = measData.slice(Math.max(measData.length - 10, 1));
            fmiDatat.push(slicedFmi[i]);
            //data.push(slicedData[i]);
        }
*/
        let notificationData = data[9];

        let notification;

        if(!notificationData) {
            notification = <div>loading</div>
        } else {
            notification = <div>{data[9].status}</div>
        }

        let fmiLoader;
        if(!this.state.fmi_data) {
            fmiLoader = <Loading />;
        }

        let northernEyeLoader;
        if(!this.state.measurement_data) {
            northernEyeLoader = <Loading />;
        }

        /*
        const series = [
            {

                name: 'Finnish Meteorological Institute total MF',
                data: fmiDatat
            },
            {
                name: 'Northern Eye total MF',
                data: data
            }
        ];

         */

        /*
        const options = {
            title: {
                text: 'EMR diagram'
            },
            xAxis: {
                categories: ['A']
            },
            series: [{
                data: data
            }]
        };
        */

        return (

        <div className="container">
                <Panel style={{ marginTop: "1rem" }}>
                    <PanelHeader><div className="row"><img id="aurora-meter" src={'./image_content/aurora-meter.svg'} alt={'Aurora-meter'} height={'30px'}></img></div></PanelHeader>
                    <div className="panel-body">
                        <ResponsiveContainer width="100%" height={350}>
                            <AreaChart data={data} margin={{ top: 25, right: 0, bottom: 0, left: 0 }}>

                                <defs>
                                    <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                                        <stop offset="5%" stopColor="#9ece72" stopOpacity={0.2}/>
                                        <stop offset="50%" stopColor="#d8d054" stopOpacity={0.1} />
                                        <stop offset="95%" stopColor="#f7bd77" stopOpacity={0}/>
                                    </linearGradient>
                                </defs>

                                <XAxis dataKey="time" tick={{fill: '#bbb', fontSize: 12}} />
                                <YAxis dataKey="totalMf" domain={[20000, 28000]} tick={{fill: '#bbb', fontSize: 12}} />

                                <Area type="monotone" name="Northern Eye total MF" dataKey="totalMf" stroke="#98ea36" fillOpacity={1} fill="url(#colorUv)" />

                                <ReferenceLine y={26000} stroke="#d8ff6d" strokeDasharray="5 5">
                                    <Label value="Increased possibility" offset={10} position="top" fill="#d8ff6d" />
                                </ReferenceLine>

                                <ReferenceLine y={27000} stroke="#ff4444" strokeDasharray="5 5">
                                    <Label value="High possibility" offset={10} position="top" fill="#ff4444" />
                                </ReferenceLine>

                                <CartesianGrid stroke="#bbb" strokeOpacity={0.15} strokeDasharray="5 5" />
                                <Legend />
                                <Tooltip />
                            </AreaChart>

                        </ResponsiveContainer>

                        <ResponsiveContainer width="100%" height={350}>
                        <AreaChart data={fmiDatat} margin={{ top: 25, right: 0, bottom: 0, left: 0 }}>

                                <defs>
                                    <linearGradient id="colorMv" x1="0" y1="0" x2="0" y2="1">
                                        <stop offset="5%" stopColor="#fd8e65" stopOpacity={0.2}/>
                                        <stop offset="50%" stopColor="#dd756f" stopOpacity={0.1} />
                                        <stop offset="95%" stopColor="#b07670" stopOpacity={0}/>
                                    </linearGradient>

                                </defs>
                                <XAxis dataKey="time" tick={{fill: '#bbb', fontSize: 12}} />
                                <YAxis dataKey="totalMf" domain={[52950, 53130]} tick={{fill: '#bbb', fontSize: 12}} />

                                <Area type="monotone" name="Finnish Meteorological Institute total MF" dataKey="totalMf" stroke="#fd8e65" fillOpacity={1} fill="url(#colorMv)" />

                                <ReferenceLine y={53070} stroke="#d8ff6d" strokeDasharray="5 5">
                                    <Label value="Increased possibility" offset={10} position="top" fill="#d8ff6d" />
                                </ReferenceLine>

                                <ReferenceLine y={53110} stroke="#ff4444" strokeDasharray="5 5">
                                    <Label value="High possibility" offset={10} position="top" fill="#ff4444" />
                                </ReferenceLine>

                                <CartesianGrid stroke="#bbb" strokeOpacity={0.15} strokeDasharray="5 5" />

                                <Legend />
                                <Tooltip />
                            </AreaChart>
                        </ResponsiveContainer>
                        {notification}
                        </div>
                    <PanelFooter />
                    </Panel>
                    </div>
        )
    }
}

export default AuroraComponent;

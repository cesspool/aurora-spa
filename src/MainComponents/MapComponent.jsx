import React, { Component } from 'react';
import Map from 'pigeon-maps';
import Marker from 'pigeon-marker';

const getProvider = () => `http://68.183.214.110:8080/styles/klokantech-basic/#11/47.379/8.5375`;

class MapComponent extends Component {

    constructor() {
        super();
        this.state = {
            position: null,
        }
    }

    render() {

        let coords = [];
        if(!this.props.position) {
            coords.push(66.4826, 25.7194);
            console.log(coords);
        } else {
            coords.push(this.props.position['coords'].latitude, this.props.position['coords'].longitude);
        }

        return (
                    <div className="panel" text="white" color="elegant-color" style={{marginTop: "1rem"}}>
                        <div className="panel-header"><div className="row"><img id="map-logo" src={'./image_content/map.svg'} alt={'Weather-location'} height={'30px'}></img></div></div>
                        <div className="panel-body" id="map-card">
                            <div id={'map'} style={{ height: '50vh', width: '100%' }}>
                                <Map center={coords} zoom={12} provider={getProvider}>
                                    <Marker anchor={coords} payload={1} onClick={({ event, anchor, payload }) => {}} />
                                </Map>
                            </div>
                        </div>
                        <div className="panel-footer">
                        </div>
                    </div>
            )
        }
    }

export default MapComponent;

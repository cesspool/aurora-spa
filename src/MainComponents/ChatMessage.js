import React from 'react'
export default ({ name, message, timeStamp  }) =>
  <div
    className="tabletti"
  >
   
    <h5><strong>{name}</strong> <em>{message}</em> <strong>{new Date().toUTCString()}</strong></h5>

  </div>
